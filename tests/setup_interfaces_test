#!/usr/bin/env atf-sh

. $(atf_get_srcdir)/test_env.sh
init_tests \
	setup_interfaces_usage \
	setup_interfaces_interactive_dhcp \
	setup_interfaces_interactive_vlan \
	setup_interfaces_interactive_vlan_ng \
	setup_interfaces_interactive_vlan0_ng \
	setup_interfaces_interactive_br0 \
	setup_interfaces_interactive_bond0 \
	setup_interfaces_interactive_wlan0

setup_interfaces_usage_body() {
	test_usage setup-interfaces
}

create_fake_ifaces() {
	local n=1
	for i; do
		mkdir -p sys/class/net/$i
		echo $n > sys/class/net/$i/ifindex
		echo down >sys/class/net/$i/operstate
		n=$((n+1))
	done
}

setup_interfaces_interactive_dhcp_body() {
	init_env
	create_fake_ifaces lo eth0
	(
		# Which one do you want to initialize? (or '?' or 'done') [eth0]
		echo eth0
		# Ip address for eth0? (or 'dhcp', 'none', '?') [dhcp]
		echo dhcp
		# Do you want to do any manual network configuration? (y/n) [n]
		echo n
	)>answers
	atf_check -s exit:0 \
		-o match:"Available interfaces are: eth0" \
		setup-interfaces <answers
}

setup_interfaces_interactive_vlan_body() {
	init_env
	create_fake_ifaces lo eth0
	(
		# Which one do you want to initialize? (or '?' or 'done') [eth0]
		echo eth0.5
		# Ip address for eth0.5? (or 'dhcp', 'none', '?') [dhcp]
		echo dhcp
		# Which one do you want to initialize? (or '?' or 'done') [eth0]
		echo done
		# Do you want to do any manual network configuration? (y/n) [n]
		echo n
	)>answers
	atf_check -s exit:0 \
		-o match:"apk add.*vlan" \
		setup-interfaces <answers
}

setup_interfaces_interactive_vlan_ng_body() {
	init_env
	create_fake_ifaces lo eth0

	mkdir -p usr/libexec/ifupdown-ng
	touch usr/libexec/ifupdown-ng/link

	(
		# Which one do you want to initialize? (or '?' or 'done') [eth0]
		echo eth0.5
		# Ip address for eth0.5? (or 'dhcp', 'none', '?') [dhcp]
		echo dhcp
		# Which one do you want to initialize? (or '?' or 'done') [eth0]
		echo done
		# Do you want to do any manual network configuration? (y/n) [n]
		echo n
	)>answers
	atf_check -s exit:0 \
		-o not-match:"apk add.*vlan" \
		setup-interfaces <answers
}

setup_interfaces_interactive_vlan0_ng_body() {
	init_env
	create_fake_ifaces lo eth0

	mkdir -p usr/libexec/ifupdown-ng
	touch usr/libexec/ifupdown-ng/link

	(
		# Which one do you want to initialize? (or '?' or 'done') [eth0]
		echo vlan0
		# Which one do you want use for vlan0? (or 'done') [eth0]
		echo eth0
		# Ip address for vlan0? (or 'dhcp', 'none', '?') [dhcp]
		echo dhcp
		# Which one do you want to initialize? (or '?' or 'done') [eth0]
		echo done
		# Do you want to do any manual network configuration? (y/n) [n]
		echo n
	)>answers
	atf_check -s exit:0 \
		-o match:"Available raw devices are: eth0" \
		-o not-match:"apk add.*vlan" \
		setup-interfaces <answers
}

setup_interfaces_interactive_br0_body() {
	init_env
	create_fake_ifaces lo eth0

	mkdir -p usr/libexec/ifupdown-ng
	touch usr/libexec/ifupdown-ng/bridge

	(
		# Which one do you want to initialize? (or '?' or 'done') [eth0]
		echo br0
		# Which port(s) do you want add to bridge br0? (or 'done') [eth0]
		echo eth0
		# Ip address for vlan0? (or 'dhcp', 'none', '?') [dhcp]
		echo dhcp
		# Which one do you want to initialize? (or '?' or 'done') [eth0]
		echo done
		# Do you want to do any manual network configuration? (y/n) [n]
		echo n
	)>answers
	atf_check -s exit:0 \
		-o match:"Available bridge ports are: eth0" \
		-o match:"apk add.*bridge" \
		setup-interfaces <answers
}

setup_interfaces_interactive_bond0_body() {
	init_env
	create_fake_ifaces lo eth0

	mkdir -p usr/libexec/ifupdown-ng
	touch usr/libexec/ifupdown-ng/bond

	(
		# Which one do you want to initialize? (or '?' or 'done') [eth0]
		echo bond0
		# Which slave(s) do you want add to bond0? (or 'done') [eth0]
		echo eth0
		# Ip address for bond0? (or 'dhcp', 'none', '?') [dhcp]
		echo dhcp
		# Do you want to do any manual network configuration? (y/n) [n]
		echo n
	)>answers
	atf_check -s exit:0 \
		-o match:"Available interfaces are: eth0" \
		-o match:"apk add.*bonding" \
		setup-interfaces <answers
}

setup_interfaces_interactive_wlan0_body() {
	init_env
	create_fake_ifaces lo eth0 wlan2
	mkdir -p sys/class/net/wlan2/phy80211

	(
		# Which one do you want to initialize? (or '?' or 'done') [eth0]
		echo wlan2
		# Type the wireless network name to connect to:
		echo Telenor0366rar
		# Type the "Telenor0366rar" network Pre-Shared Key (will not echo):
		echo 0123456789
		# Ip address for wlan0? (or 'dhcp', 'none', '?') [dhcp]
		echo dhcp
		# Do you want to do any manual network configuration? (y/n) [n]
		echo n
	)>answers
	atf_check -s exit:0 \
		-o match:"Available interfaces are: eth0 wlan2" \
		-o match:"Type the \"Telenor0366rar\" network Pre-Shared Key" \
		-o match:"Ip address for wlan2" \
		setup-interfaces <answers
}
